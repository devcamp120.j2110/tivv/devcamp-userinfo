const gUserInfo = {
  firstname: 'Hoang',
  lastname: 'Pham',
  avatar: 'https://cdn.icon-icons.com/icons2/1378/PNG/512/avatardefault_92824.png',
  age: 30,
  language: ['Vietnamese', 'Japanese', 'English']
}

function App() {
  return (
    <div>
      <h5>
        Họ và tên: {gUserInfo.firstname + " " + gUserInfo.lastname} 
      </h5>
      <img src={gUserInfo.avatar} width='300px'/>
      <p>
        Tuổi: {gUserInfo.age} <br/>
        {gUserInfo.age > 35 ? "Anh ấy đã già" : " Anh ấy còn trẻ"}
      </p>
      <ul>
        {gUserInfo.language.map((element,index) => <li key={index}> {element} </li>)}
      </ul>
    </div>
  );
}

export default App;
